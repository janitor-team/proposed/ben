THIS_PATH=$PWD

BEN_PATH=/usr/share/doc/ben
BEN=/usr/bin/ben

# Detect if we are running from a git checkout
if [ -d $(dirname $0)/../../.git ]; then
    BEN_PATH=$(readlink -f $(dirname $0)/../..)
    BEN=$BEN_PATH/ben
fi

ben () {
    $BEN "$@"
}

download () {
    cd $THIS_PATH
    mkdir -p $1 && ( cd $1 && ben download -c $BEN_PATH/examples/download/$1.ben --use-cache )
}

update () {
    cd $THIS_PATH
    download testing
    download unstable
    mkdir -p new
    cd new
    echo Downloading excuses.yaml...
    curl --silent https://release.debian.org/britney/excuses.yaml > excuses.yaml
    echo Parsing excuses.yaml...
    $BEN_PATH/examples/migrate/extract-verdicts.py excuses.yaml > verdicts.txt
    cd ..
}

migrate () {
    cd $THIS_PATH/new
    ben migrate ../testing/ben.cache ../unstable/ben.cache "$@"
    cd ..
}

debcheck () {
    cd $THIS_PATH
    for u in testing/Packages_*; do
        u=${u##*/}
        echo "=====> $u <=====";
        diff -u <(dose-debcheck --failures --explain testing/$u) <(dose-debcheck --failures --explain new/$u)
    done
}
