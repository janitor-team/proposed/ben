#!/usr/bin/env python3

import sys
import yaml

excuses = yaml.safe_load(open(sys.argv[1]))

for source in excuses["sources"]:
    print("{} {}".format(source["item-name"], source["migration-policy-verdict"]))
