val frontend : Benl_frontend.frontend

type output_format = Text | Xhtml | Levels | Json

val string_of_format : output_format -> string
val format_of_string : string -> output_format

type transition_data = {
    config : Benl_types.expr Benl_core.StringMap.t;
    monitor_data :
      ([ `source ] Package.t *
         (string * (string * Benl_base.status)) list)
        list list;
    sources : ([ `source ], [ `source ] Package.t) Package.Map.t;
    binaries : [ `binary ] Package.t Benl_modules.PAMap.t;
    rounds : [ `source ] Package.Name.t list list;
    dep_graph : ([ `source ], [ `source ] Package.Set.t) Package.Map.t;
    all : int;
    bad : int;
    packages : [ `source ] Benl_data.S.t;
  }

val compute_transition_data :
  Benl_modules.Marshallable.t ->
  Benl_types.expr Benl_core.StringMap.t ->
  transition_data

val print_html_monitor :
  Template.t ->
  transition_data ->
  [< Html_types.div_content_fun > `Table ] Tyxml.Html.elt option ->
  Tyxml.Html.doc

val print_monitor :
  output_format -> Format.formatter -> transition_data -> unit

val baseurl : string ref
val a_link : string -> string Tyxml.Html.wrap -> [> `A of [> `PCDATA ] ] Tyxml.Html.elt
val generated_on_text : unit -> [> `A of [> `PCDATA ] | `PCDATA ] Tyxml.Html.elt list
val check_media_dir : string -> unit
