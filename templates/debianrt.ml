open Printf
open Tyxml.Html
open Benl_compat.Html

let href uri =
  a_href (uri_of_string uri)

let a_link url text =
  a ~a:[a_href (uri_of_string url)] [pcdata text]

let page ~title ~subtitle ~headers ~body ~footer =
  let headers =
    (meta
      ~a:[a_charset "utf-8"]
      ())
    ::
    (link ~rel:[`Stylesheet] ~href:"media/revamp.css" ())
    ::
    (link ~rel:[`Stylesheet] ~href:"media/styles.css" ())
    ::
    headers in
  html ~a:[a_xmlns `W3_org_1999_xhtml]
    (head
       (Tyxml.Html.title (pcdata title))
       headers
    )
    (Tyxml.Html.body ~a:[a_class ["debian"]] [
      h1 ~a:[a_id "title"]
        [a_link "https://release.debian.org/" "Debian Release Management"];
      h2 ~a:[a_id "subtitle"] subtitle;
      div ~a:[a_id "body"] body;
      div ~a:[a_id "footer"] footer
    ])

open Template

let () =
  Benl_templates.register_template {
    name = "Debian";
    page;
    intro = [
      b [ a_link
            "https://wiki.debian.org/Teams/ReleaseTeam/Transitions"
            "Transition documentation"
        ];
      br ();
      b [ a_link
            "https://bugs.debian.org/cgi-bin/pkgreport.cgi?users=release.debian.org@packages.debian.org;tag=transition"
            "Bugs tagged \"transition\""
        ];
      br ();
      br ();
    ];
    pts = (fun ~src -> sprintf "https://tracker.debian.org/%s" src);
    changelog = (fun ~letter:_ ~src ~ver:_ -> sprintf "https://packages.debian.org/changelog:%s" src);
    buildd = (fun ~src ~ver:_ -> sprintf "https://buildd.debian.org/status/package.php?p=%s" src);
    buildds = (fun ~srcs ->
      let srcs = String.concat "," srcs in
      Some (sprintf "https://buildd.debian.org/status/package.php?p=%s&compact=compact" srcs));
    bugs = (fun ~src -> sprintf "https://bugs.debian.org/%s" src);
    critical_bugs = (fun ~srcs ->
      let srcs = String.concat ";src=" srcs in
      Some (sprintf "https://bugs.debian.org/cgi-bin/pkgreport.cgi?sev-inc=serious;sev-inc=grave;sev-inc=critical;src=%s" srcs));
    msg_id = (fun ~mid -> sprintf "https://lists.debian.org/%s" mid);
  }
